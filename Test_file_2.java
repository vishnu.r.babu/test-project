import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class StringUtilsTest {

    @Test
    public void testReverseString() {
        // Arrange
        String input = "Hello World";
        
        // Act
        String reversed = StringUtils.reverse(input);
        
        // Assert
        assertEquals("dlroW olleH", reversed);
    }
    
    @Test
    public void testReverseStringWithEmptyString() {
        // Arrange
        String input = "";
        
        // Act
        String reversed = StringUtils.reverse(input);
        
        // Assert
        assertEquals("", reversed);
    }
    
    @Test
    public void testReverseStringWithNull() {
        // Arrange
        String input = null;
        
        // Act
        String reversed = StringUtils.reverse(input);
        
        // Assert
        assertNull(reversed);
    }
}
