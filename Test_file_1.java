import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class MyUnitTest {

    @Test
    public void testAddition() {
        // Arrange
        int a = 5;
        int b = 10;
        
        // Act
        int result = a + b;
        
        // Assert
        assertEquals(15, result);
    }
}
